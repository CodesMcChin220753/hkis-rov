package srv

import (
	"fmt"
	"github.com/shipp02/go-rov/msg"
	zmq "github.com/zeromq/goczmq"
	"log"
	"strconv"
)

//TODO:Pusher does not work? Mystery
type Pusher struct {
	sock sender
}

func NewPusher(port int) *Pusher {
	pSock, err := zmq.NewPush("tcp://127.0.0.1:" + strconv.Itoa(port))
	if err != nil {
		log.Println(err)
		return nil
	}
	return &Pusher{sock: pSock}
}

func MakePusher(sock sender) *Pusher {
	return &Pusher{sock: sock}
}

func (p *Pusher) Send(msg msg.IPushPull) {
	bin := []byte(fmt.Sprint(msg))
	err := p.sock.SendMessage([][]byte{bin})
	if err != nil {
		log.Println("Pusher error: ", err)
		return
	}
}

func (p *Pusher) Destroy() {
	destroy(p.sock)
}
